package edu.msoft.customerms;

public record CustomerFullHistory(Customer customer, EarningsReport earnings) { }
