# Talleres - MSOFT - Desarrollo de Aplicaciones Empresariales

## Licencia JetBrains

Registrarse mediante el siguiente enlace con su correo electrónico académico y obtener su cuenta de Jetbrains for Students.

https://www.jetbrains.com/shop/eform/students

## Software necesario
- [Java Development Kit 21](https://bell-sw.com/pages/downloads/#jdk-21-lts)
- [JetBrains Toolbox](https://www.jetbrains.com/toolbox-app/)
  - Instalar
  - Acceder con su cuenta de Jetbrains
  - Descargar/Instalar IntelliJ IDEA Ultimate
  - Si tiene problemas con la asignación de la licencia estudiantil, entonces instalar IntelliJ IDEA Community
- Git
- [Docker Desktop](https://docs.docker.com/get-docker/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)
- [Lens](https://k8slens.dev/download)
- [Postman](https://www.postman.com/downloads/)

## Contenido

### Backend
- [Taller 00 - SOAP Service](t00-soap)
- [Taller 01 - REST Service](t01-rest)
- [Taller 02 - Events](t02-events)

### Microservicios (Spring Cloud) 
- [Taller 10 - Config Server](t10-config-server)
- [Taller 11 - Discovery Server (Config First)](t11-discovery-server)
- [Taller 12 - Discovery Server (Discovery First)](t12-discovery-first)
- [Taller 13 - Client Side Load Balancing](t13-client-side-load-balancing)
- [Taller 14 - API Gateway](t14-api-gateway)
- [Taller 15 - Distributed Tracing](t15-distributed-tracing)

### Microservicios (K8s)
- [Taller 30 - Kubernetes](t30-k8s)
- [Taller 31 - Kubernetes (Observability)](t31-k8s-observability)

### Seguridad en Aplicaciones Web
- [Taller 20 - MTLS](t20-mtls)
- [Taller 21 - MTLS (Spring Boot)](t21-mtls-sb)
- [Taller 22 - JWT](t22-jwt)
- [Taller 23 - OIDC (Client Credentials)](t23-oidc-clientcredentials)
- [Taller 24 - OIDC (Authorization Code + PKCE)](t24-oidc-authcode-pkce)
